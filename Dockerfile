FROM openjdk:18-jdk-alpine
COPY target/*.jar child-microservice.jar
ENTRYPOINT ["java", "-jar", "child-microservice.jar"]
