package com.example.childmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChildMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChildMicroserviceApplication.class, args);
    }

}
