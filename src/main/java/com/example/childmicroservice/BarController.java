package com.example.childmicroservice;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class BarController {

    @GetMapping("/bar")
    public ResponseEntity<String> bar(@RequestParam String query) {
        if ("a".equals(query)) {
            return ResponseEntity.internalServerError().build();
        } else {
            return ResponseEntity.ok(String.format("bar query: %s", query));
        }
    }

}
